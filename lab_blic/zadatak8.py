import sys

if len(sys.argv) < 2:
    print("Nedovoljno argumenata")
    exit()

with open(sys.argv[1], 'r', encoding='utf8') as dat:
    logs = dat.readlines()

byteSize = {}

for log in logs:

    IPaddr = log.split('.')
    size = log.split()[9]
    if size == '-':
        continue
    elif IPaddr[0] + '.' + IPaddr[1] in byteSize.keys():
        byteSize[IPaddr[0] + '.' + IPaddr[1]] += int(size)
    else:
        byteSize[IPaddr[0] + '.' + IPaddr[1]] = int(size)

sortedByteSize = {k: v for k, v in sorted(byteSize.items(), key=lambda item: item[1], reverse=True)}

print('-'*30)
print(' IP adrese | Br. pristupa')
print('-'*30)

for key in sortedByteSize.keys():
    print('{:>7s}.*.*{:>8s}'.format(key, str(sortedByteSize[key])))
print('-'*30)
